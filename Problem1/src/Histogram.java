
public class Histogram
{
  private int[] array;
  private int lineLength, arrayLength, start, max, perElement;

  public Histogram(int arrayLength, int start, int perElement, int lineLength)
  {
    this.lineLength = lineLength;
    this.arrayLength = arrayLength;
    this.start = start;
    this.perElement = perElement;
    max = arrayLength * perElement + start;
    array = new int[Math.abs(arrayLength)];
  }

  public String toString()
  {
    String output = "";
    int largestValue = 0;
    int padding = Math.max((start + "").length(), (max + "").length());

    for (int i = 0; i < arrayLength; i++)
      largestValue = Math.max(array[i], largestValue);

    output += "One * is roughly equal to ";
    output += (double) largestValue / lineLength;
    output += " values.\n\n\n";

    for (int i = 0; i < arrayLength; i++)
    {
      int firstNumber = i * perElement + start;
      int secondNumber = i * perElement + perElement + start - 1;
      output += String.format("%1$" + padding + "s", firstNumber + "");
      output += "  to  ";
      output += String.format("%1$-" + padding + "s", secondNumber + "");
      output += "     " + asterisks(array[i] * lineLength / largestValue);
      output += '\n';
    }
    return output;
  }

  public void countInt(int iCount)
  {
    iCount = Math.max(iCount, start);
    iCount = Math.min(iCount, max);
    array[(iCount - start) / perElement]++;
  }

  private String asterisks(int asteriskNumber)
  {
    String output = "";
    for (int i = 1; i <= asteriskNumber; i++)
      output += '*';
    return output;
  }
}
