
//********************************************************************
//File:         Driver.java       
//Author:       Kalvin Dewey
//Date:         11/11/17
//Course:       CPS100
//
//Problem Statement:
//Write a program that creates a histogram that allows you to
//visually inspect the frequency distribution of a set of values. The
//program should read in an arbitrary number of integers from a
//text input file that are in the range 1 to 100 inclusive; then pro-
//duce a chart similar to the one below that indicates how many
//input values fell in the range 1 to 10, 11 to 20, and so on.
//Scale the data so the longest histogram line is 60 characters long
//(the same scaling factor should be used for each line).
//
//Inputs: realdata.txt
//Outputs:  a histogram
// 
//********************************************************************

import java.util.Scanner;
import java.io.*;


public class Driver
{
  public static void main(String[] args) throws IOException
  {
    final int ARRAY_LENGTH = 10, MIN = 1, PER_ELEMENT = 10, LINE_LENGTH = 60;

    Scanner fileScan = new Scanner(new File("realdata.txt"));
    Histogram histogram = new Histogram(ARRAY_LENGTH, MIN, PER_ELEMENT,
        LINE_LENGTH);

    while (fileScan.hasNext())
    {
      histogram.countInt(fileScan.nextInt());
    }
    
    System.out.println(histogram);

    fileScan.close();
  }

}
